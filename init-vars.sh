export PSCI_GROUP=$(echo $CI_PROJECT_PATH | cut -d"/" -f2)
export PSCI_PRJ=$(echo $CI_PROJECT_PATH | cut -d"/" -f3)
export PSCI_DATE=$(date +%y%m%d.%H%M%S -d @`git show -s --format=%at $CI_COMMIT_SHA`)
export PSCI_TAG=${CI_COMMIT_SHA:0:7}
export PSCI_BRANCH=$CI_COMMIT_REF_SLUG
export PSCI_NAMESPACE=$PSCI_GROUP
export PSCI_VERSION=$PSCI_DATE.$PSCI_TAG

export PSCI_IMAGE_PATH="$PSCI_HARBOR_URL/$PSCI_GROUP/$PSCI_PRJ"
export PSCI_SUB_DOMAIN="$CI_PROJECT_PATH_SLUG-$CI_ENVIRONMENT_SLUG"
export PSCI_FULL_DOMAIN="$CI_PROJECT_PATH_SLUG-$CI_ENVIRONMENT_SLUG.$AUTO_DEVOPS_DOMAIN"

echo "-------------------------------------------------------------------------"
echo "PSCI_GROUP:"$PSCI_GROUP
echo "PSCI_PRJ:"$PSCI_PRJ
echo "PSCI_DATE:"$PSCI_DATE
echo "PSCI_TAG:"$PSCI_TAG
echo "PSCI_BRANCH:"$PSCI_BRANCH
echo "PSCI_NAMESPACE:"$PSCI_NAMESPACE
echo "PSCI_VERSION:"$PSCI_VERSION

echo "PSCI_IMAGE_PATH:"$PSCI_IMAGE_PATH
echo "PSCI_SUB_DOMAIN:"$PSCI_SUB_DOMAIN
echo "PSCI_FULL_DOMAIN:"$PSCI_FULL_DOMAIN
echo "-------------------------------------------------------------------------"
