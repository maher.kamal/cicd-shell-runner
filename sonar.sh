#!/bin/bash
set -e

if [ "$PSCI_BRANCH" == "master" ]
then
  /home/gitlab-runner/sonar/bin/sonar-scanner -Dsonar.host.url=https://sonar.progressoft.io
fi
