apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: {{ include "app.fullname" . }}
  labels:
    app.kubernetes.io/name: {{ include "app.name" . }}
    helm.sh/chart: {{ include "app.chart" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "app.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "app.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
      imagePullSecrets:
      - name: harbor-{{ include "app.fullname" . }}
      {{ if .Values.database.db_engine.mssql.enabled }}
      volumes:
      - name: init-mssql
        configMap:
          name: {{ template "app.fullname" . }}-mssql-init
          items:
          - key: init-mssql.sql
            path: init-mssql.sql
      {{ end }}
      {{ if .Values.database.db_engine.mssql.enabled }}
      initContainers:
      - name: init-mssql
        image: mcr.microsoft.com/mssql-tools
        command: ["/bin/sh","-c"]
        args: ["/opt/mssql-tools/bin/sqlcmd -S $(SQL_SERVER) -U sa -P $(SQL_PASSWORD) -i /tmp/init-mssql.sql"]
        volumeMounts:
        - mountPath: /tmp
          name: init-mssql
        env:
        - name: SQL_SERVER
          value: "{{ .Release.Name }}-db"
        - name: SQL_PASSWORD
          value: "P@ssw0rd"
      {{ end }}
      containers:
      - name: {{ .Chart.Name }}
        image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        ports:
        - containerPort: {{ .Values.service.internalPort }}
        env:
        - name: "JAVA_OPTS"
        {{- if .Values.apm.enabled }}
          value: {{ .Values.tomcat.javaOpts }} -javaagent:/usr/local/tomcat/elastic-apm-agent.jar -Delastic.apm.service_name={{ .Release.Name }}-app -Delastic.apm.server_urls={{ .Values.apm.serviceUrl }}
        {{- else }}
          value: {{ .Values.tomcat.javaOpts }}
        {{- end }}
        - name: "env_broker_url"
          value: tcp://{{ .Release.Name }}-activemq-broker:61616
        {{ if .Values.database.db_engine.oracle.enabled }}
        - name: "db_url"
          value: jdbc:oracle:thin:@{{ .Values.database.db_url }}:1521:xe
        - name: "db_user"
          value: {{ .Values.database.oracle.db_user }}
        - name: "db_password"
          value: {{ .Values.database.oracle.db_password }}
        - name: "db_schema"
          value: {{ .Values.database.oracle.db_schema }}
        - name: "db_type"
          value: {{ .Values.database.oracle.db_type }}
        - name: "db_driver"
          value: {{ .Values.database.oracle.db_driver }}
        - name: "db_validation_query"
          value: {{ .Values.database.oracle.db_validation_query }}
        - name: "db_dialect"
          value: {{ .Values.database.oracle.db_dialect }}
        - name: "db_check_query"
          value: {{ .Values.database.oracle.db_check_query }}
      {{ else if .Values.database.db_engine.h2.enabled }}
        - name: "db_url"
          value: jdbc:h2:tcp://{{ .Values.database.db_url }}:1521/jfw;MVCC=true;LOCK_TIMEOUT=5000
        - name: "db_user"
          value: {{ .Values.database.h2.db_user }}
        - name: "db_password"
          value: {{ .Values.database.h2.db_password }}
        - name: "db_schema"
          value: {{ .Values.database.h2.db_schema }}
        - name: "db_type"
          value: {{ .Values.database.h2.db_type }}
        - name: "db_driver"
          value: {{ .Values.database.h2.db_driver }}
        - name: "db_validation_query"
          value: {{ .Values.database.h2.db_validation_query }}
        - name: "db_dialect"
          value: {{ .Values.database.h2.db_dialect }}
        - name: "db_check_query"
          value: {{ .Values.database.h2.db_check_query }}
      {{ else if .Values.database.db_engine.mssql.enabled }}
        - name: "db_url"
          value: jdbc:jtds:sqlserver://{{ .Values.database.db_url }}:1433/jfw
        - name: "db_user"
          value: {{ .Values.database.mssql.db_user }}
        - name: "db_password"
          value: {{ .Values.database.mssql.db_password }}
        - name: "db_schema"
          value: {{ .Values.database.mssql.db_schema }}
        - name: "db_type"
          value: {{ .Values.database.mssql.db_type }}
        - name: "db_driver"
          value: {{ .Values.database.mssql.db_driver }}
        - name: "db_validation_query"
          value: {{ .Values.database.mssql.db_validation_query }}
        - name: "db_dialect"
          value: {{ .Values.database.mssql.db_dialect }}
        - name: "db_check_query"
          value: {{ .Values.database.mssql.db_check_query }}
        {{ end }}
        # livenessProbe:
        #   httpGet:
        #     path: /
        #     port: http
        # readinessProbe:
        #   httpGet:
        #     path: /
        #     port: http
          resources:
{{ toYaml .Values.resources | indent 12 }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
    {{ toYaml . | indent 8 }}
    {{- end }}
      {{- with .Values.affinity }}
          affinity:
        {{ toYaml . | indent 8 }}
        {{- end }}
          {{- with .Values.tolerations }}
              tolerations:
            {{ toYaml . | indent 8 }}
            {{- end }}
