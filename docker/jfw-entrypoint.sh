#!/usr/bin/env bash

if [ ! -f db_init_status ]; then
    echo "false" > db_init_status
fi
export DB_INITIALIZE=$(cat db_init_status)

echo "..............DB Initializing Checking"

if [ "$DB_INITIALIZE" = "false" ]; then
    echo "...................DB Initializing Starting"
    /usr/local/tomcat/db-init.sh
    /usr/local/tomcat/app-initialize.sh
    echo "true" > db_init_status
    export DB_INITIALIZE=$(cat db_init_status)
    echo "..................DB Initializing end"
fi
catalina.sh jpda run
