#!/bin/bash

max_retries=300
retry_count=0
sleep_secs=2
ready_pods=false

echo "..........Polling for DB readiness"
if [ ! -f /usr/local/tomcat/webapps/ROOT/WEB-INF/lib/db-test-*.jar ]; then
   echo "db-test jar not found"
   ls -l /usr/local/tomcat/webapps/ROOT/WEB-INF/lib/db-test*
   exit 1
fi

while [ "$ready_pods" != "true" ] && [ "$retry_count" -lt "$max_retries" ]; do
    ready_pods=$(java -cp /usr/local/tomcat/webapps/ROOT/WEB-INF/lib/db-test-*.jar com.progressoft.jfw.dbtest.Test)
    retry_count=$((retry_count + 1))
    sleep "$sleep_secs"
done

if [ "$ready_pods" != "true" ]; then
    echo "ERROR: Timeout waiting for DB to start up." >&2
    exit 1
fi
echo "..........DB Engine is UP!"


export start=$(date +%s)

echo "..........Initializing DB"
cd /usr/local/tomcat/webapps/ROOT/WEB-INF/lib

if [ "$LOG_JFW_INITIALIZER" == "true" ]; then
        java -jar initializer-*.jar ${ENTITIES_PATH}
else
        java -jar initializer-*.jar ${ENTITIES_PATH}
fi

export end=$(date +%s)

cd /usr/local/tomcat/bin

echo "..........DB schema is Ready!" for Database $db_type in $((end-start)) Seconds
